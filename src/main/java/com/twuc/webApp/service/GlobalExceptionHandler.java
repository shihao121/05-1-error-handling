package com.twuc.webApp.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Map<String, String>> handleIllegalArgument(IllegalArgumentException exception) {
        HashMap<String, String> exceptionMap = new HashMap<>();
        exceptionMap.put("message", "Something wrong with brother or sister.");
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(exceptionMap);
    }

}
