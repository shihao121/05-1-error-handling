package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
public class UserController {

    @GetMapping(value = "/api/errors/illegal-argument")
    public String throwException() {
        throw new RuntimeException("Something wrong with the argument");
    }

    @GetMapping(value = "/api/exception/access-control-exception")
    public String throwAccessControlException() {
        throw new AccessControlException("Something was wrong with AccessControl");
    }

    @GetMapping(value = "/api/errors/null-pointer")
    public void nullPoint() {
        throw new NullPointerException("null-point");
    }

    @GetMapping(value = "/api/errors/arithmetic")
    public void arithmetic() {
        throw new ArithmeticException("arithmeticException");
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<String> handleMultipleException (NullPointerException nullException,
                                                           ArithmeticException arithmeticException) {
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Something wrong with the argument");

    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleException(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
    }

    @ExceptionHandler(AccessControlException.class)
    public ResponseEntity<String> handleAccessException(RuntimeException exception) {
        return ResponseEntity.status(403).body(exception.getMessage());
    }


}
