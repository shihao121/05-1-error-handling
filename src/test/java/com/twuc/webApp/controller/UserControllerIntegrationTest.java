package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class UserControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void should_return_500_with_errormessage_when_throw_exception() {
        ResponseEntity<String> entity = restTemplate.getForEntity("/api/errors/illegal-argument", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody());
    }

    @Test
    void should_return_403_with_errormessage_when_throw_access_control_exception() {
        ResponseEntity<String> entity = restTemplate.getForEntity("/api/exception/access-control-exception", String.class);
        assertEquals(403, entity.getStatusCodeValue());
        assertEquals("Something was wrong with AccessControl", entity.getBody());
    }

//    @Test
//    void should_return_418_status_with_message_when_throw_multiple_exception() {
//        ResponseEntity<String> entity = restTemplate.getForEntity("/api/errors/null-pointer", String.class);
//        ResponseEntity<String> arithmeticEntity = restTemplate.getForEntity(" /api/errors/arithmetic", String.class);
//        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
//        assertEquals(HttpStatus.I_AM_A_TEAPOT, arithmeticEntity.getStatusCode());
//    }

    @Test
    void should_return_418_status_with_message_use_global_exception_handler() {
        ResponseEntity<String> sisterEntity = restTemplate.getForEntity("/api/sister-errors/illegal-argument", String.class);
        ResponseEntity<String> brotherEntity = restTemplate.getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, sisterEntity.getStatusCode());
        assertEquals(HttpStatus.I_AM_A_TEAPOT, brotherEntity.getStatusCode());
        assertEquals("{\"message\":\"Something wrong with brother or sister.\"}", sisterEntity.getBody());
        assertEquals("{\"message\":\"Something wrong with brother or sister.\"}", brotherEntity.getBody());
    }
}