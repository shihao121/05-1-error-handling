package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BrotherController {

    @GetMapping(value = "/api/brother-errors/illegal-argument")
    public void illegalArgument() {
        throw new IllegalArgumentException("-> <-");
    }
}
