package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SisterController {

    @GetMapping(value = "/api/sister-errors/illegal-argument")
    public void IllegalThrow() {
        throw new IllegalArgumentException("=v=");
    }

}
